package main

import (
	"context"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"time"
)

type Message struct {
	CurrentTime time.Time
	Text        string
}

var (
	MsgChan      = make(chan Message)
	Ctx, Cancel  = context.WithTimeout(context.Background(), 1*time.Minute)
	TimeInterval = 5 * time.Second
)

func main() {
	defer Cancel()

	go StartSender()
	go StartReceiver()

	// sleep is just to wait for routines to do their work
	for {
		time.Sleep(10 * time.Minute)
	}
}

func StartSender() {
	ticker := time.NewTicker(TimeInterval)
	for {
		select {
		case <-ticker.C:
			MsgChan <- Message{CurrentTime: time.Now(), Text: "Some very important info :)"}
		case <-Ctx.Done():
			fmt.Println("Sender Closed!")
			return
		}
	}
}

func StartReceiver() {
	for {
		select {
		case msg := <-MsgChan:
			fmt.Printf("CurrentTime: %v | Message: %v \n", msg.CurrentTime.String(), msg.Text)
		case <-Ctx.Done():
			fmt.Println("Receiver Closed!")
			return
		}
	}
}

func init() {
	cfg, err := os.Open("./config.yml")
	if err != nil {
		fmt.Println("[WARNING] Config file not found | Program started with default values")
		return
	}
	byteValue, _ := ioutil.ReadAll(cfg)

	var config map[string]interface{}
	err = yaml.Unmarshal(byteValue, &config)
	if err != nil {
		fmt.Println("[WARNING] Config file unmarshaling failed | Program started with default values")
		return
	}

	if v, ok := config["TIME_INTERVAL"].(int); ok {
		TimeInterval = time.Duration(v) * time.Second
	} else {
		fmt.Println("[WARNING] Config values is invalid | Program started with default values")
	}
}
